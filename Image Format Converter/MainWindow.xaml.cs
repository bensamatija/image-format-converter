﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Drawing.Imaging;
using DR = System.Drawing;

namespace Image_Format_Converter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            LoadDefaultSettings();
            this.Title = appName;
        }

        string appName = "Images Format Converter v.1.1.0.";

        int stringLenghtLimit = 150;
        string filename;

        public void ConvertImagesInSubfolders()
        {
            // Prepare Set:
            string sourcePath = ImagesLocation.Text;
            string outputPath = ImagesDestination.Text;
            IEnumerable<string> subFolders = Directory.EnumerateDirectories(sourcePath, "*", SearchOption.TopDirectoryOnly);

            // Set Encoder:
            //System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
            //EncoderParameter encoderParameters = new EncoderParameter(encoder, 50L);

            try
            {
                foreach (var dir in subFolders)
                {
                    IEnumerable<string> imgs = Directory.EnumerateFiles(dir, "*.*", SearchOption.TopDirectoryOnly);
                    string currentFolder = System.IO.Path.GetFileName(dir);
                    Directory.CreateDirectory(outputPath + "\\" + currentFolder);
                    try
                    {
                        foreach (string img in imgs)
                        {
                            filename = System.IO.Path.GetFileNameWithoutExtension(img);
                            //var original = DR.Image.FromFile(img);
                            string outputFilename = outputPath + "\\" + currentFolder + "\\" + filename + ".JPEG";

                            try
                            {
                                // Test string lenght limit:
                                if (outputFilename.Length >= stringLenghtLimit + 10)
                                {
                                    string trimmedName = outputFilename.Remove(outputFilename.Length - (outputFilename.Length - stringLenghtLimit));
                                    outputFilename = trimmedName + ".JPEg";
                                }
                                else
                                {
                                    //outputFilename = outputPath + "\\" + currentFolder + "\\" + filename + ".JPEG";
                                }
                            }
                            catch (Exception e)
                            {
                                //MessageBox.Show(e.Message, e.Source);
                            }

                            try
                            {
                                //var original = DR.Image.FromFile(img);
                                //original.Save(outputFilename, ImageFormat.Jpeg);

                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    using (FileStream fileStream = new FileStream(outputFilename, FileMode.Create, FileAccess.ReadWrite))
                                    {
                                        try
                                        {
                                            var original = DR.Image.FromFile(img);

                                            original.Save(memoryStream, ImageFormat.Jpeg);
                                            byte[] bytes = memoryStream.ToArray();
                                            fileStream.Write(bytes, 0, bytes.Length);
                                        }
                                        catch (Exception e)
                                        {
                                            //MessageBox.Show(e.Message, e.Source);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                //MessageBox.Show(e.Message, e.Source);
                            }
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                        }
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show(e.Message, e.Source);
                    }
                }
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message, e.Source);
            }
        }


        private void Convert_Click(object sender, RoutedEventArgs e)
        {
            ConvertImagesInSubfolders();
        }

        public void LoadDefaultSettings()
        {
            ImagesLocation.Text = Properties.Settings.Default.ImagesLocation;
            ImagesDestination.Text = Properties.Settings.Default.ImagesDestination;
        }

        public void SaveDefaultSettings()
        {
            Properties.Settings.Default.ImagesLocation = ImagesLocation.Text;
            Properties.Settings.Default.ImagesDestination = ImagesDestination.Text;
            Properties.Settings.Default.Save();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDefaultSettings();
        }


    }
}
